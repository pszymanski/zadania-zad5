#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime


print "Content-Type: text/html"
print

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
    </body>
</html>""" % (
    os.environ.get('SERVER_NAME','P7'), # Nazwa serwera
    '194.29.175.240', # IP serwera
    '31007', # port serwera
    '', # nazwa klienta
    '', # IP klienta
    '', # port klienta
    '', # nazwa skryptu
    datetime.datetime.now().strftime("%H:%M:%S"), # czas
)

print body
