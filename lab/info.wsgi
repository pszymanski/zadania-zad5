#!/usr/bin/python
# -*- coding=utf-8 -*-

import datetime
import Cookie

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""

def application(environ, start_response):
    cookie=Cookie.SimpleCookie()
    lastdate='Pierwsza wizyta'
    if environ.has_key('HTTP_COOKIE'):
        cookie.load(environ['HTTP_COOKIE'])
        #print cookie
        lastdate=cookie['lastdate'].value
    response_body = body % (
         environ.get('SERVER_NAME', 'P7'), # nazwa serwera
         '194.29.175.240', # IP serwera
         environ['SERVER_PORT'], # port serwera
         environ.get('REMOTE_USER','Unset'), # nazwa klienta
         environ['REMOTE_ADDR'], # IP klienta
         environ.get('REMOTE_PORT','Unset'), # port klienta
         environ['SCRIPT_NAME'], # nazwa skryptu
         datetime.datetime.now().strftime("%H:%M:%S"), # bieżący czas
         lastdate, # czas ostatniej wizyty
    )
    cookie['lastdate']=datetime.datetime.now().strftime("%H:%M:%S")
    print cookie['lastdate']
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ("Set-Cookie", 'lastdate='+cookie['lastdate'].value),
                        ('Content-Length', str(len(response_body))),
                        ]
    start_response(status, response_headers)

    return [response_body]

class Upperware:
        def __init__(self, app):
            self.wrapped_app = app

        def __call__(self, environ, start_response):
            for data in self.wrapped_app(environ, start_response):
                yield data.upper()


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    ap=Upperware(application)
    srv = make_server('', 31007, ap)
    srv.serve_forever()
